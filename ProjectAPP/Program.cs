﻿// See https://aka.ms/new-console-template for more information

using ProjectAPP.Model;
using ProjectAPP.Repository;

ProductRepository productRepository = new ProductRepository();
Product[] allProducts = productRepository.GetAllProducts();
Console.WriteLine("Enter category : ");
string category = Console.ReadLine();
Product[] catProducts = productRepository.GetCategory(category);

//foreach(Product item in allProducts)
//{
//    Console.WriteLine($"{item.Name}");
//}
//Console.WriteLine(catProducts);

foreach (Product item in catProducts)
{
    Console.WriteLine($"{item.Name}");
}

Console.WriteLine("Name of product to be deleted : ");
string  productName = Console.ReadLine();

productRepository.deleteProduct(productName);
allProducts = productRepository.GetAllProducts();
foreach (Product item in allProducts)
{
    Console.WriteLine($"{item.Name}");
}