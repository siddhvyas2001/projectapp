﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectAPP.Repository;

namespace ProjectAPP.Model
{
    internal class Product
    {
        public string Name;
        public string Category;
        public float Rating;
        public int Price;

        public Product(string name, string category, int price, float rating)
        {
            Name = name;
            Category = category;
            Rating = rating;
            Price = price;
        }
    }
}
