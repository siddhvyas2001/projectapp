﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectAPP.Model;

namespace ProjectAPP.Repository
{
    internal class ProductRepository
    {
        Product[] products;
        Product[] categorizedProducts;
        Product[] finalCategorizedProducts;
        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Oppo", "Mobile", 15000, 4.2f),
                new Product("Vivo", "Mobile", 15700, 4.3f),
                new Product("Lenovo", "Mobile", 16000, 4.3f),
                new Product("Apple", "Laptop", 75000, 4.0f),
                new Product("Samsung", "TV", 25000, 4.4f),
            };
        }

        internal void deleteProduct(string? productName)
        {
            int deletedIndex = 0;
            for(int i = 0; i<products.Length; i++)
            {
                if(products[i].Name == productName)
                {
                    deletedIndex = i;
                    break;
                }
            }
            for(int i = deletedIndex; i<products.Length-1; i++)
            {
                products[i] = products[i + 1];
            }
        }

        internal Product[] GetCategory(string? category)
        {
            int count = 0;
            var totallength = products.Length;
            categorizedProducts = new Product[totallength];
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].Category == category)
                {
                    categorizedProducts[count] = products[i];
                    count++;
                }
            }
            //int newlen = 0;
            //for (int i = 0; i < categorizedProducts.Length; i++)
            //{
            //    if (categorizedProducts[i] == null)
            //    {
            //        newlen = i + 1;
            //    }

            //} 
            //for(int i = 0; i<newlen; i++)
            //{
            //    finalCategorizedProducts[i] = categorizedProducts[i];
            //}
            return categorizedProducts;
        }

        internal Product[] GetAllProducts()
        {
            return products;
        }
    }
}
